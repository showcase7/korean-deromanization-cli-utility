# Korean deromanization CLI utility
## Description
This project was made in order to let me input 한글 (Korean script) without having to switch keyboard layout and learn a new one.

The program converts text written in 'romanized' hangeul into hangeul. It can operate either in batch or interactive mode. 

In batch mode (when given an argument), it converts the given text and puts it in the clipboard. It treats items separated by whitespace individually, unless the item is quoted.

In interactive mode, the utility shows the resulting hangeul from the entered text, and copies the hangeul to the clipboard when enter is pressed.

The utility has some options to work with the OSX buil-in dictionary and the Anki flashcard learning tool, for when I was using those in my learning.

## Documentation
In order to see what latin characters are converted to each hangeul character, see the auto-generated documentation of the module. This can be done my invoking `cargo doc --open`.

This also includes documentation about using the code as a Rust library.

## Bulding the program
The program requires the Rust project manager `cargo`, and an internet connection to fetch dependencies.

The program can be built using `cargo build --release`. This puts the resulting executable at `target/release/dero(.exe)`.

## Running the program
The program can be run from the binary, or through `cargo` as `cargo run --release --`. 

To run the program on a small piece of text, run it as
`dero "annyeox haseyo"`.

To run the program in interactive mode, run it as 
`dero`.

The other options can be found using `dero --help`.

## License
This project is available under the Universal Permissive License, Version 1.0 ([LICENSE.txt]) or https://oss.oracle.com/licenses/upl/ .
